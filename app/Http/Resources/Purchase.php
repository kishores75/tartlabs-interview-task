<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\User;
use App\Models\Product;
use App\Models\Service;

class Purchase extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_detail' => ($this->user_id) ? User::find($this->user_id)->getUserName()->get() : "",
            'purchase_detail' => ($this->purchaseable_type == 'App\Models\Product') ? Product::find($this->purchaseable_id)->getProductName()->get() : Service::find($this->purchaseable_id)->getServiceName()->get(),
            // 'service_detail' => ($this->purchaseable_id) ? Service::find($this->purchaseable_id)->getServiceName()->get() : "",
            // 'product_detail' => ($this->purchaseable_id) ? Product::find($this->purchaseable_id)->getProductName()->get() : "",
            'created_at' => $this->created_at->format('d-m-Y'),
            'updated_at' => $this->updated_at->format('d-m-Y'),
        ];
    }
}