<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use App\Events\SendMail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ServiceController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:service-list|service-create|service-edit|service-delete', ['only' => ['index','show']]);
        $this->middleware('permission:service-create', ['only' => ['create','store']]);
        $this->middleware('permission:service-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:service-delete', ['only' => ['destroy']]);
        $this->middleware('auth');
    }

    public function index()
    {
        $services = Service::latest()->paginate(20);
        return view('services.index',compact('services'))
            ->with('i', (request()->input('page', 1) - 1) * 20);
    }

    public function create()
    {
        return view('services.create');
    }
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'detail' => 'required',
            'file' => 'image|mimes:jpg,jpeg,png,gif,svg|max:2048'
        ]);
        $imgPath1 = '';
        $imgName1 = $request->file('file');
        if($imgName1){
            $imgPath1 = strtolower(date('Y-m-d').'_'.microtime(true).'_'.$imgName1->getClientOriginalName());
            // $path = $imgName1->storeAs('public/uploads/services', $imgPath1);
            // Storage::disk('public')->put('/uploads/services', $imgPath1);
            // Storage::putFile('/public/uploads/services', $imgName1);

            Storage::putFileAs('/public/uploads/services', $imgName1, $imgPath1);
        
            $destinationPath = storage_path('/app/public/uploads/services_thumbnails');
            $img = Image::make($imgName1->path());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$imgPath1);

            // $img = Image::make($imgName1->path());
            // $img->orientate();
            // $img->resize(100, 100, function ($constraint){
            //     $constraint->aspectRatio();
            // });
            // Storage::putFileAs('/public/uploads/services_thumbnails', $imgName1, $img);

        }
        // Service::create($request->all());
        
        Service::create([
            'name' => $request->input('name'),
            'detail' => $request->input('detail'),
            'image' => $imgPath1,
        ]);
        return redirect()->route('services.index')->with('success','Service created successfully.');
    }

    public function show(Service $service)
    {
        return view('services.show',compact('service'));
    }

    public function edit(Service $service)
    {
        return view('services.edit',compact('service'));
    }
    public function update(Request $request, Service $service)
    {
        request()->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
        
        $imgPath1 = $request->input('oldFile');
        $imgName1 = $request->file('file');
        if($imgName1){
            // Storage::delete($imgPath1);
            Storage::disk('public')->delete('/uploads/services/'.$imgPath1);
            Storage::disk('public')->delete('/uploads/services_thumbnails/'.$imgPath1);

            // unlink(storage_path('app//public/uploads/services/'.$imgPath1));

            $imgPath1 = strtolower(date('Y-m-d').'_'.microtime(true).'_'.$imgName1->getClientOriginalName());
            Storage::putFileAs('/public/uploads/services', $imgName1, $imgPath1);
        
            $destinationPath = storage_path('/app/public/uploads/services_thumbnails');
            $img = Image::make($imgName1->path());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$imgPath1);
        }
        Service::where('id', $service->id)
        ->update([
            'name' => $request->input('name'),
            'detail' => $request->input('detail'),
            'image' => $imgPath1,
        ]);
        return redirect()->route('services.index')->with('success','Service updated successfully');
    }

    public function destroy(Service $service)
    {
        $services = Service::findOrFail($service->id);
        if($services) {
            Storage::disk('public')->delete('/uploads/services/'.$services->image);
            Storage::disk('public')->delete('/uploads/services_thumbnails/'.$services->image);
            $service->delete();
            return redirect()->route('services.index')->with('success','Service deleted successfully');
        }
        return redirect()->route('services.index')->with('success','Something Went Wrong');
    }
    
    public function purchase(Request $request, $id)
    {
        $purchase_id = Service::find($id);
        
        $user = auth()->user();
        $user_id = $user->id;
        
        $purchase_id->purchases()->create([
            'user_id' => $user_id,
            'status' => 'success',
        ]);
        
        Event::dispatch(new SendMail($user_id));

        return redirect()->route('services.index')->with('success','Purchased successfully.');
    }
}