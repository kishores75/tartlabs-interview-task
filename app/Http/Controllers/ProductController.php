<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use App\Events\SendMail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index','show']]);
        $this->middleware('permission:product-create', ['only' => ['create','store']]);
        $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy']]);
        $this->middleware('auth');
    }

    public function index()
    {
        $products = Product::latest()->paginate(20);
        return view('products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 20);
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'detail' => 'required',
            'file' => 'image|mimes:jpg,jpeg,png,gif,svg|max:2048'
        ]);
        $imgPath1 = '';
        $imgName1 = $request->file('file');
        if($imgName1){
            $imgPath1 = strtolower(date('Y-m-d').'_'.microtime(true).'_'.$imgName1->getClientOriginalName());
            // $path = $imgName1->storeAs('public/uploads/products', $imgPath1);
            // Storage::disk('public')->put('/uploads/products', $imgPath1);
            // Storage::putFile('/public/uploads/products', $imgName1);

            Storage::putFileAs('/public/uploads/products', $imgName1, $imgPath1);
        
            $destinationPath = storage_path('/app/public/uploads/products_thumbnails');
            $img = Image::make($imgName1->path());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$imgPath1);

            // $img = Image::make($imgName1->path());
            // $img->orientate();
            // $img->resize(100, 100, function ($constraint){
            //     $constraint->aspectRatio();
            // });
            // Storage::putFileAs('/public/uploads/products_thumbnails', $imgName1, $img);

        }
        // Product::create($request->all());
        
        Product::create([
            'name' => $request->input('name'),
            'detail' => $request->input('detail'),
            'image' => $imgPath1,
        ]);
        return redirect()->route('products.index')->with('success','Product created successfully.');
    }
    public function show(Product $product)
    {
        return view('products.show',compact('product'));
    }

    public function edit(Product $product)
    {
        return view('products.edit',compact('product'));
    }

    public function update(Request $request, Product $product)
    {
        request()->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
        
        $imgPath1 = $request->input('oldFile');
        $imgName1 = $request->file('file');
        if($imgName1){
            // Storage::delete($imgPath1);
            Storage::disk('public')->delete('/uploads/products/'.$imgPath1);
            Storage::disk('public')->delete('/uploads/products_thumbnails/'.$imgPath1);

            // unlink(storage_path('app//public/uploads/products/'.$imgPath1));

            $imgPath1 = strtolower(date('Y-m-d').'_'.microtime(true).'_'.$imgName1->getClientOriginalName());
            Storage::putFileAs('/public/uploads/products', $imgName1, $imgPath1);
        
            $destinationPath = storage_path('/app/public/uploads/products_thumbnails');
            $img = Image::make($imgName1->path());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$imgPath1);
        }
        Product::where('id', $product->id)
        ->update([
            'name' => $request->input('name'),
            'detail' => $request->input('detail'),
            'image' => $imgPath1,
        ]);
        return redirect()->route('products.index')->with('success','Product updated successfully');
    }

    public function destroy(Product $product)
    {
        $products = Product::findOrFail($product->id);
        if($products) {
            Storage::disk('public')->delete('/uploads/products/'.$products->image);
            Storage::disk('public')->delete('/uploads/products_thumbnails/'.$products->image);
            $product->delete();
            return redirect()->route('products.index')->with('success','Product deleted successfully');
        }
        return redirect()->route('products.index')->with('success','Something Went Wrong');
    }

    public function purchase(Request $request, $id)
    {
        $purchase_id = Product::find($id);
        
        $user = auth()->user();
        $user_id = $user->id;
        
        $purchase_id->purchases()->create([
            'user_id' => $user_id,
            'status' => 'success',
        ]);
        
        Event::dispatch(new SendMail($user_id));

        return redirect()->route('products.index')->with('success','Purchased successfully.');
    }
}