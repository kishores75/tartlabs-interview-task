<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [ 'name', 'image', 'detail' ];

    protected $guarded = [];
    
    public function getServiceName(){
        return $this->hasOne(Service::class, 'id')->select('name', 'detail');
    }
    public function purchases(){
        return $this->MorphMany(Purchase::class, 'purchaseable');
    }
}