<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [ 'name', 'image', 'detail' ];

    protected $guarded = [];

    public function getProductName(){
        return $this->hasOne(Product::class, 'id')->select('name','detail');
    }
    public function purchases(){
        return $this->MorphMany(Purchase::class, 'purchaseable');
    }
}